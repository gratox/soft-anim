/**
 * \file lecteur.cpp
 * \brief Lecteur princiaple du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#include "lecteur.h"

Lecteur::Lecteur()
{
    this->setFixedHeight(200);
    this->setFrameShape(QFrame::Panel);

    //les widgets
    lbl_TempsEcoule = new QLabel(tr("Temps écoulé", "Dans les lecteurs principaux"));
    lbl_TempsRestant = new QLabel(tr("Temps restant", "Dans les lecteurs principaux"));
    lbl_TempsTotal = new QLabel(tr("Temps total", "Dans les lecteurs principaux"));

    txt_TempsEcoule = new QLabel("00:00:000");
    txt_TempsRestant = new QLabel("00:00:000");
    txt_TempsTotal = new QLabel("00:00:000");

    txt_TitreArtiste = new QLabel ("none - none / none");

    sld_Position = new QSlider (Qt::Horizontal);

    btn_Play = new QPushButton (tr("Play/Pause"));
    btn_Play->setFixedHeight(80);
    btn_Stop = new QPushButton (tr("Stop"));
    btn_Stop->setFixedHeight(80);

    //les layouts
    hLayout1 = new QHBoxLayout();
    hLayout2 = new QHBoxLayout();
    hLayout3 = new QHBoxLayout();
    vLayout = new QVBoxLayout();

    hLayout1->addWidget(lbl_TempsEcoule);
    hLayout1->addWidget(lbl_TempsRestant);
    hLayout1->addWidget(lbl_TempsTotal);

    hLayout2->addWidget(txt_TempsEcoule);
    hLayout2->addWidget(txt_TempsRestant);
    hLayout2->addWidget(txt_TempsTotal);

    hLayout3->addWidget(btn_Play);
    hLayout3->addWidget(btn_Stop);

    vLayout->addLayout(hLayout1);
    vLayout->addLayout(hLayout2);
    vLayout->addWidget(txt_TitreArtiste);
    vLayout->addWidget(sld_Position);
    vLayout->addLayout(hLayout3);

    this->setLayout(vLayout);
}
