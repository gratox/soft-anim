/**
 * \file horloge.cpp
 * \brief Horloge du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#include "horloge.h"

Horloge::Horloge()
{
    this->setFixedSize(130, 70);
    this->setFrameShape(QFrame::Panel);
    lbl_heure = new QLabel("00:00:00");
    lbl_heure->setFont(QFont("Comic Sans MS", 20));

    vLayout = new QVBoxLayout();
    vLayout->addWidget(lbl_heure);
    this->setLayout(vLayout);

    time = new QTime();
    timer = new QTimer();

    timer->start(250);
    connect (timer, SIGNAL(timeout()), this, SLOT(OnTimer()));
}

void Horloge::OnTimer()
{
    //a chaque tick du timer mise à jour de l'heure
    lbl_heure->setText(time->currentTime().toString("hh:mm:ss"));
}
