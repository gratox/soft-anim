/**
 * \file horloge.h
 * \brief Horloge du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#ifndef HORLOGE_H
#define HORLOGE_H

#include <QtGui>

class Horloge : public QFrame
{
    Q_OBJECT

    public:
        Horloge();

    private slots:
        void OnTimer(void);

    private:
        //label pour afficher l'heure
        QLabel *lbl_heure;
        QTime *time;
        QTimer *timer;
        QVBoxLayout *vLayout;
};

#endif // HORLOGE_H
