/**
 * \file preecoute.cpp
 * \brief lecteur de préécoute du logiciel
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#include "preecoute.h"

PreEcoute::PreEcoute()
{
    this->setFixedHeight(70);
    this->setFrameShape(QFrame::Panel);

    //layout
    vLayout1 = new QVBoxLayout();
    vLayout2 = new QVBoxLayout();
    vLayout3 = new QVBoxLayout();
    hLayout = new QHBoxLayout();

    //widget
    lbl_TempsEcoule = new QLabel (tr("Temps écoulé", "Dans lecteur de pré écoute"));
    lbl_TempsTotal = new QLabel (tr("Temps total", "Dans lecteur de pré écoute"));
    txt_TempsEcoule = new QLabel ("00:00:00");
    txt_TempsTotal = new QLabel ("00:00:00");
    txt_TitreArtiste = new QLabel ("none - none / none");
    btn_Play = new QPushButton("Play/Pause");
    btn_Play->setFixedSize(100, 50);
    sld_Position = new QSlider(Qt::Horizontal);

    //ajout dans layout
    vLayout1->addWidget(txt_TitreArtiste);
    vLayout1->addWidget(sld_Position);

    vLayout2->addWidget(lbl_TempsEcoule);
    vLayout2->addWidget(lbl_TempsTotal);

    vLayout3->addWidget(txt_TempsEcoule);
    vLayout3->addWidget(txt_TempsTotal);

    hLayout->addLayout(vLayout1);
    hLayout->addLayout(vLayout2);
    hLayout->addLayout(vLayout3);
    hLayout->addWidget(btn_Play);

    this->setLayout(hLayout);
}
