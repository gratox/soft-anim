SOURCES += \
    main.cpp \
    fenetreprincipale.cpp \
    lecteur.cpp \
    horloge.cpp \
    preecoute.cpp

HEADERS += \
    fenetreprincipale.h \
    lecteur.h \
    horloge.h \
    preecoute.h
