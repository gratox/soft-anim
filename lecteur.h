/**
 * \file lecteur.h
 * \brief Lecteur princiaple du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#ifndef LECTEUR_H
#define LECTEUR_H

#include <QtGui>

class Lecteur : public QFrame
{
    Q_OBJECT

    public:
        Lecteur();

    private:
        //label
        QLabel *lbl_TempsEcoule, *lbl_TempsRestant, *lbl_TempsTotal;
        QLabel *txt_TempsEcoule, *txt_TempsRestant, *txt_TempsTotal;
        QLabel *txt_TitreArtiste;
        //bouton
        QPushButton *btn_Play, *btn_Stop;
        //slider
        QSlider *sld_Position;
        //layout pour les widgets
        QHBoxLayout *hLayout1, *hLayout2, *hLayout3;
        QVBoxLayout *vLayout;
};

#endif // LECTEUR_H
