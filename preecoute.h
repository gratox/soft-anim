/**
 * \file preecoute.h
 * \brief lecteur de préécoute du logiciel
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#ifndef PREECOUTE_H
#define PREECOUTE_H

#include <QtGui>

class PreEcoute : public QFrame
{
    Q_OBJECT

    public:
        PreEcoute();

    private:
        //layout
        QVBoxLayout *vLayout1, *vLayout2, *vLayout3;
        QHBoxLayout *hLayout;
        //widget
        QLabel *lbl_TempsEcoule, *lbl_TempsTotal;
        QLabel *txt_TempsEcoule, *txt_TempsTotal;
        QLabel *txt_TitreArtiste;
        //bouton
        QPushButton *btn_Play;
        //slider
        QSlider *sld_Position;
};

#endif // PREECOUTE_H
