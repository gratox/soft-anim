/**
 * \file fenetreprincipale.h
 * \brief Fenetre principale du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QtGui>
#include "lecteur.h"
#include "horloge.h"
#include "preecoute.h"

class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT

    public:
        FenetrePrincipale();
    
    signals:
    
    public slots:
    
    private:
        //conteneur pour les widgets
        QHBoxLayout *hLayout1, *hLayout2;
        QVBoxLayout *vLayout;
        QWidget *centralWidget;

        //widgets vides pour imitation fenetre
        Lecteur *lecteur1, *lecteur2;
        QWidget *crossfader;
        Horloge *horloge;
        PreEcoute *preecoute;

        //pour mettre les longlets en bas de la fenêtre
        QTabWidget *zoneOnglet;
};

#endif // FENETREPRINCIPALE_H
