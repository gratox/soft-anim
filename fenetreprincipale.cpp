/**
 * \file fenetreprincipale.cpp
 * \brief Fenetre principale du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#include "fenetreprincipale.h"

FenetrePrincipale::FenetrePrincipale ()
{
    //création des layouts
    hLayout1 = new QHBoxLayout ();
    hLayout2 = new QHBoxLayout ();
    vLayout = new QVBoxLayout ();
    centralWidget = new QWidget();

    //widgets vides pour imitation fenetre
    lecteur1 = new Lecteur();
    lecteur2 = new Lecteur();

    crossfader = new QWidget();
    crossfader->setFixedSize(300, 200);
    crossfader->setStyleSheet("background: #FF55CC");

    horloge = new Horloge();

    preecoute = new PreEcoute();

    //pour mettre les longlets en bas de la fenêtre
    zoneOnglet = new QTabWidget();
    zoneOnglet->setMinimumHeight(300);

    hLayout1->addWidget(lecteur1);
    hLayout1->addWidget(crossfader);
    hLayout1->addWidget(lecteur2);

    hLayout2->addWidget(horloge);
    hLayout2->addWidget(preecoute);

    vLayout->addLayout(hLayout1);
    vLayout->addLayout(hLayout2);
    vLayout->addWidget(zoneOnglet);

    centralWidget->setLayout(vLayout);
    this->setCentralWidget(centralWidget);
}
