/**
 * \file main.cpp
 * \brief point d'entrée du programme
 *
 * \author FORTIN Guillaume
 *
 * \date 16/09/2012
 * \version 1
 */

#include <QApplication>
#include "fenetreprincipale.h"

int main (int argc, char **argv)
{
    QApplication app (argc, argv);

    //pour les accents
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    //ouvrir la fenêtre principale du programme
    FenetrePrincipale *fen = new FenetrePrincipale();
    fen->show();

    return app.exec();
}
